%bcond_with snapshot_build

%if %{with snapshot_build}
# Unlock LLVM Snapshot LUA functions
%{llvm_sb_verbose}
%{llvm_sb}
%endif

# Opt out of https://fedoraproject.org/wiki/Changes/fno-omit-frame-pointer
# https://bugzilla.redhat.com/show_bug.cgi?id=2158587
%undefine _include_frame_pointers

%bcond_with compat_build

%global maj_ver 18
%global min_ver 1
%global patch_ver 8
#global rc_ver 4
%if %{with snapshot_build}
%global maj_ver %{llvm_snapshot_version_major}
%global min_ver %{llvm_snapshot_version_minor}
%global patch_ver %{llvm_snapshot_version_patch}
%undefine rc_ver
%endif
%global compiler_rt_version %{maj_ver}.%{min_ver}.%{patch_ver}

%global crt_srcdir compiler-rt-%{compiler_rt_version}%{?rc_ver:rc%{rc_ver}}.src

%if %{with compat_build}
%global pkg_name compiler-rt%{maj_ver}
%else
%global pkg_name compiler-rt
%endif

# see https://sourceware.org/bugzilla/show_bug.cgi?id=25271
%global optflags %(echo %{optflags} -D_DEFAULT_SOURCE)

# see https://gcc.gnu.org/bugzilla/show_bug.cgi?id=93615
%global optflags %(echo %{optflags} -Dasm=__asm__)

Name:		%{pkg_name}
Version:	%{compiler_rt_version}%{?rc_ver:~rc%{rc_ver}}%{?llvm_snapshot_version_suffix:~%{llvm_snapshot_version_suffix}}
Release:	1%{?dist}
Summary:	LLVM "compiler-rt" runtime libraries

License:	NCSA or MIT
URL:		http://llvm.org
%if %{with snapshot_build}
Source0:    %{llvm_snapshot_source_prefix}compiler-rt-%{llvm_snapshot_yyyymmdd}.src.tar.xz
%{llvm_snapshot_extra_source_tags}
%else
Source0:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{compiler_rt_version}%{?rc_ver:-rc%{rc_ver}}/%{crt_srcdir}.tar.xz
Source1:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{compiler_rt_version}%{?rc_ver:-rc%{rc_ver}}/%{crt_srcdir}.tar.xz.sig
Source2:	release-keys.asc
%endif

Patch0:		0001-compiler-rt-Fix-FLOAT16-feature-detection.patch

# RHEL-specific patches
Patch100:	0001-Drop-fno-stack-protector-from-the-compiler-flags.patch
Patch101:	0001-Fix-page-size-constant-on-aarch64-and-ppc64le.patch	

BuildRequires:	clang
BuildRequires:	cmake
BuildRequires:	ninja-build
BuildRequires:	python3
# We need python3-devel for %%py3_shebang_fix
BuildRequires:	python3-devel
BuildRequires:	llvm-devel = %{version}
BuildRequires:	llvm-cmake-utils = %{version}

# For gpg source verification
BuildRequires:	gnupg2

Requires: clang-resource-filesystem%{?isa} = %{version}
Provides: %{name}(major) = %{maj_ver}

%description
The compiler-rt project is a part of the LLVM project. It provides
implementation of the low-level target-specific hooks required by
code generation, sanitizer runtimes and profiling library for code
instrumentation, and Blocks C language extension.

%prep
%{gpgverify} --keyring='%{SOURCE2}' --signature='%{SOURCE1}' --data='%{SOURCE0}'
%autosetup -n %{crt_srcdir} -p2

# compiler-rt does not allow configuring LLVM_COMMON_CMAKE_UTILS.
ln -s %{_datadir}/llvm/cmake ../cmake

%py3_shebang_fix lib/hwasan/scripts/hwasan_symbolize

%build
%undefine __cmake_in_source_build

# Copy CFLAGS into ASMFLAGS, so -fcf-protection is used when compiling assembly files.
export ASMFLAGS="%{build_cflags}"

# RHEL 8 specific: "global toolchain clang" is not supported on RHEL 8, so explicitly enable
# clang in cmake. This is necessary to get the correct float16 ABI.
%cmake	-GNinja \
	-DCMAKE_BUILD_TYPE=RelWithDebInfo \
	-DCMAKE_MODULE_PATH=%{_libdir}/cmake/llvm \
	-DCMAKE_SKIP_RPATH:BOOL=ON \
	-DCMAKE_C_COMPILER=clang \
	-DCMAKE_CXX_COMPILER=clang++ \
	-DCOMPILER_RT_INSTALL_PATH=%{_prefix}/lib/clang/%{maj_ver} \
	-DLLVM_ENABLE_PER_TARGET_RUNTIME_DIR=ON \
	\
%if %{with snapshot_build}
	-DLLVM_VERSION_SUFFIX="%{llvm_snapshot_version_suffix}" \
%endif
	\
%if 0%{?__isa_bits} == 64
	-DLLVM_LIBDIR_SUFFIX=64 \
%else
	-DLLVM_LIBDIR_SUFFIX= \
%endif
	-DCOMPILER_RT_INCLUDE_TESTS:BOOL=OFF # could be on?

%cmake_build

%install

%cmake_install
%ifarch ppc64le
# Fix install path on ppc64le so that the directory name matches the triple used
# by clang.
mv %{buildroot}%{_prefix}/lib/clang/%{maj_ver}/lib/powerpc64le-redhat-linux-gnu %{buildroot}%{_prefix}/lib/clang/%{maj_ver}/lib/ppc64le-redhat-linux-gnu
%endif
%ifarch %{ix86}
# Fix install path on ix86 so that the directory name matches the triple used
# by clang on both actual ix86 and on x86_64 with -m32:
%if "%{_target_cpu}" != "i386"
ln -s i386-redhat-linux-gnu %{buildroot}%{_prefix}/lib/clang/%{maj_ver}/lib/%{_target_cpu}-redhat-linux-gnu
%endif
%endif

%check
#%%cmake_build --target check-compiler-rt

%files
%license LICENSE.TXT
%ifarch x86_64 aarch64
%{_prefix}/lib/clang/%{maj_ver}/bin/*
%endif
%{_prefix}/lib/clang/%{maj_ver}/include/*
%{_prefix}/lib/clang/%{maj_ver}/lib/*
%{_prefix}/lib/clang/%{maj_ver}/share/*
#%ifarch x86_64 aarch64
#{_bindir}/hwasan_symbolize
#%endif

%changelog
* Tue Jul 09 2024 Tom Stellard <tstellar@redhat.com> - 18.1.8-1
- 18.1.8 Release

* Fri Mar 22 2024 Tom Stellard <tstellar@redhat.com> - 18.1.2-1
- 18.1.2 Release

* Tue Mar 12 2024 Tom Stellard <tstellar@redhat.com> - 18.1.1-1
- 18.1.1 Release

* Wed Feb 28 2024 Tom Stellard <tstellar@redhat.com> - 18.1.0~rc4-1
- 18.1.0-rc4 Release

* Wed Jan 24 2024 Fedora Release Engineering <releng@fedoraproject.org> - 17.0.6-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Fri Jan 19 2024 Fedora Release Engineering <releng@fedoraproject.org> - 17.0.6-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

%{?llvm_snapshot_changelog_entry}

* Mon Dec 18 2023 Jeremy Newton <alexjnewt at hotmail dot com> - 17.0.6-3
- Add compiler-rt(major) provides

* Wed Dec 13 2023 Miro Hrončok <mhroncok@redhat.com> - 17.0.6-2
- Fix install path on i686

* Wed Nov 29 2023 Nikita Popov <npopov@redhat.com> - 17.0.6-1
- Update to LLVM 17.0.6

* Fri Oct 13 2023 Nikita Popov <npopov@redhat.com> - 17.0.2-2
- Build with clang

* Wed Oct 04 2023 Nikita Popov <npopov@redhat.com> - 17.0.2-1
- Update to LLVM 17.0.2

* Fri Jun 23 2023 Tom Stellard <tstellar@redhat.com> - 16.0.6-1
- 16.0.6 Release

* Thu Apr 13 2023 Tom Stellard <tstellar@redhat.com> - 16.0.0-1
- Update to LLVM 16.0.0

* Thu Jan 19 2023 Tom Stellard <tstellar@redhat.com> - 15.0.7-1
- Update to LLVM 15.0.7

* Tue Sep 06 2022 Nikita Popov <npopov@redhat.com> - 15.0.0-1
- Update to LLVM 15.0.0

* Tue Jun 28 2022 Tom Stellard <tstellar@redhat.com> - 14.0.6-1
- 14.0.6 Release

* Wed May 25 2022 Timm Bäder <tbaeder@redhat.com> - 14.0.0-3
- Fix page size constant size on aarch64 and ppc64le

* Fri Apr 29 2022 Timm Bäder <tbaeder@redhat.com> - 14.0.0-2
- Remove llvm-cmake-devel BR

* Thu Apr 07 2022 Timm Bäder <tbaeder@redhat.com> - 14.0.0-1
- Update to 14.0.0

* Thu Feb 03 2022 Tom Stellard <tstellar@redhat.com> - 13.0.1-1
- 13.0.1 Release

* Fri Oct 15 2021 Tom Stellard <tstellar@redhat.com> - 13.0.0-1
- 13.0.0 Release

* Fri Jul 16 2021 sguelton@redhat.com - 12.0.1-1
- 12.0.1 release

* Tue May 25 2021 sguelton@redhat.com - 12.0.0-2
- Backport several compatibility patches

* Thu May 6 2021 sguelton@redhat.com - 12.0.0-1
- 12.0.0 release

* Thu Oct 29 2020 sguelton@redhat.com - 11.0.0-1
- 11.0.0 final release

* Mon Sep 21 2020 sguelton@redhat.com - 11.0.0-0.1.rc2
- 11.0.0-rc2 Release

* Fri Jul 24 2020 sguelton@redhat.com - 10.0.1-1
- 10.0.1 release

* Mon Jun 15 2020 sguelton@redhat.com - 10.0.0-2
- Fix msan compilation warnings, see rhbz#1841165

* Wed Apr 8 2020 sguelton@redhat.com - 10.0.0-1
- 10.0.0 final

* Mon Jan 06 2020 Tom Stellard <tstellar@redhat.com> - 9.0.1-2
- Update fno-stack-protector patch to apply with -p2

* Fri Dec 20 2019 Tom Stellard <tstellar@redhat.com> - 9.0.1-1
- 9.0.1 Release

* Fri Sep 27 2019 Tom Stellard <tstellar@redhat.com> - 9.0.0-1
- 9.0.0 Release

* Thu Aug 1 2019 sguelton@redhat.com - 8.0.1-1
- 8.0.1 release

* Thu Jul 4 2019 sguelton@redhat.com - 8.0.1-0.2.rc2
- Fix rhbz#1678240

* Thu Jun 13 2019 sguelton@redhat.com - 8.0.1-0.1.rc2
- 8.0.1rc2 Release

* Wed Apr 17 2019 sguelton@redhat.com - 8.0.0-1
- 8.0.0 Release 

* Fri Dec 14 2018 Tom Stellard <tstellar@redhat.com> - 7.0.1-1
- 7.0.1 Release 

* Mon Dec 10 2018 Tom Stellard <tstellar@redhat.com> - 7.0.1-0.1.rc3
- 7.0.1-rc3 Release 

* Tue Nov 27 2018 Tom Stellard <tstellar@redhat.com> - 7.0.0-1
- 7.0.0 Release 

* Tue Oct 02 2018 Tom Stellard <tstellar@redhat.com> - 6.0.1-5
- Use python3 for build scripts

* Mon Oct 01 2018 Tom Stellard <tstellar@redhat.com> - 6.0.1-4
- Drop scl macros

* Thu Sep 06 2018 Tom Stellard <tstellar@redhat.com> - 6.0.1-3
- Drop -fno-stack-protector flag

* Thu Sep 06 2018 Tom Stellard <tstellar@redhat.com> - 6.0.1-2
- Explicitly BuildRequire: /usr/bin/python3

* Wed Jul 11 2018 Tom Stellard <tstellar@redhat.com> - 6.0.1-1
- 6.0.1 Release

* Tue Jan 09 2018 Tom Stellard <tstellar@redhat.com> - 5.0.1-1
- 5.0.1 Release

* Wed Jun 07 2017 Tom Stellard <tstellar@redhat.com> - 4.0.1-1
- 4.0.1 Release

* Wed Jun 07 2017 Tom Stellard <tstellar@redhat.com> - 4.0.0-3
- Build for llvm-toolset-7 rename

* Thu May 18 2017 Tom Stellard <tstellar@redhat.com> - 4.0.0-2
- Fix disabling debug on s390(x)

* Tue Mar 14 2017 Tom Stellard <tstellar@redhat.com> - 4.0.0-1
- compiler-rt 4.0.0 Final Release

* Thu Mar 02 2017 Dave Airlie <airlied@redhat.com> - 3.9.1-1
- compiler-rt 3.9.1

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.9.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Nov 21 2016 Dan Horák <dan[at]danny.cz> - 3.9.0-3
- disable debuginfo on s390(x)

* Wed Nov 02 2016 Dave Airlie <airlied@redhat.com> - 3.9.0-2
- build for new arches.

* Wed Oct 26 2016 Dave Airlie <airlied@redhat.com> - 3.9.0-1
- compiler-rt 3.9.0 final release

* Mon May  2 2016 Tom Callaway <spot@fedoraproject.org> 3.8.0-2
- make symlinks to where the linker thinks these libs are

* Thu Mar 10 2016 Dave Airlie <airlied@redhat.com> 3.8.0-1
- compiler-rt 3.8.0 final release

* Thu Mar 03 2016 Dave Airlie <airlied@redhat.com> 3.8.0-0.2
- compiler-rt 3.8.0rc3

* Thu Feb 18 2016 Dave Airlie <airlied@redhat.com> - 3.8.0-0.1
- compiler-rt 3.8.0rc2

* Fri Feb 05 2016 Dave Airlie <airlied@redhat.com> 3.7.1-3
- fix compiler-rt paths - from rwindz0@gmail.com - #1304605

* Wed Feb 03 2016 Fedora Release Engineering <releng@fedoraproject.org> - 3.7.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Tue Oct 06 2015 Jan Vcelak <jvcelak@fedoraproject.org> 3.7.0-100
- initial version using cmake build system
